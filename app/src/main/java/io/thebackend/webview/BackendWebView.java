package io.thebackend.webview;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;

import android.os.Handler;
import android.webkit.JavascriptInterface;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.unity3d.player.UnityPlayerActivity;

public class BackendWebView extends UnityPlayerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("ssss","ssssss");

    }
    private final Handler handler = new Handler();

    private static final BackendWebView ourInstance = new BackendWebView();

    protected static final String LOGTAG = "BACKENDLOG";

    public static  BackendWebView getInstance() {return ourInstance; }

    public static Activity mainActivity;

    private String clientAppId = "";
    private String gamerIndate = "";

    private String webURLIndex = "file:///android_asset/www/index.html";

    private String webURLList = "file:///android_asset/www/list.html";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(LOGTAG,"sss");
    }

    private class AndroidBridge
    {
        @JavascriptInterface
        public void ClosePanel()
        {
            handler.post(new Runnable(){
                @Override
                public void run(){
                    closeWebView();
                }
            });
        }

        @JavascriptInterface
        public void OpenGallery()
        {
            handler.post(new Runnable(){
                @Override
                public void run(){
                    openGallery();
                }
            });
        }

        @JavascriptInterface
        public String GetGameId() {
            return clientAppId;
        }
        @JavascriptInterface
        public String GetGamerIndate() {
            return gamerIndate;
        }

        @JavascriptInterface
        public void ChangeIndexScene()
        {
            webView.post(new Runnable(){
                @Override
                public void run(){
                    webView.loadUrl(webURLIndex);
                }
            });
        }

        @JavascriptInterface
        public void ChangeListScene()
        {
            webView.post(new Runnable(){
                @Override
                public void run(){
                    webView.loadUrl(webURLList);
                }
            });
        }
    }

    private LinearLayout webLayout;
    public ObservableWebView webView;
    private WebSettings mWebSettings;

    private BackendWebView()
    {
        Log.i(LOGTAG, "Created BackendWebViewPlugin");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void openWebView(final String _clientAppId, final String _gamerIndate, final int left, final int top, final int right, final int bottom) {
        try
        {
            clientAppId = _clientAppId;
            gamerIndate = _gamerIndate;

            runOnUiThread(new Runnable() {
                public void run() {
                    if(webLayout ==null)

                {
                    webLayout = new LinearLayout(mainActivity);
                    webLayout.setOrientation(LinearLayout.VERTICAL);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    layoutParams.setMargins(left, top, right, bottom);

                    mainActivity.addContentView(webLayout, layoutParams);

                    if (webView == null)
                        webView = new ObservableWebView(mainActivity);
                    webView.setOnScrollChangedCallback(new ObservableWebView.OnScrollChangedCallback() {
                        @Override
                        public void onScroll(int l, int t) {
                            if (webView.getUrl().equals(webURLList)) {
                                int tek = (int) Math.floor(webView.getContentHeight() * webView.getScale());
                                if (tek - webView.getScrollY() <= webView.getHeight() + 5) {
                                    BackendWebView.getInstance().webView.loadUrl("javascript:handleGetQuestionList()");
                                }
                            }
                        }
                    });
                    webView.setWebChromeClient(new WebChromeClient() {
                    });
                    layoutParams.weight = 1.0f;
                    webView.setLayoutParams(layoutParams);
                    webView.loadUrl(webURLIndex);
                    mWebSettings = webView.getSettings();
                    mWebSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
                    mWebSettings.setJavaScriptEnabled(true);
                    mWebSettings.setAllowFileAccessFromFileURLs(true);
                    mWebSettings.setAllowUniversalAccessFromFileURLs(true);
                    webView.addJavascriptInterface(new AndroidBridge(), "WebView");

                    webLayout.addView(webView);
                }
                    else

                {
                    LogError("showWebView", "WebView is already open");
                }

            }
        });
        }
        catch(Exception e)
        {
            LogError("showWebView", e.toString());
            e.printStackTrace();
        }
    }

    public void closeWebView() {
        try
        {
            mainActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (webLayout != null) {
                        webLayout.removeAllViews();
                        webLayout.setVisibility(View.GONE);
                        webLayout = null;
                        webView = null;
                    }
                }
            });
        }
        catch(Exception e)
        {
            LogError("closeWebView", e.toString());
            e.printStackTrace();
        }

    }

    public void openGallery()
    {
        checkSelfPermission();

        openView();
    }
    public void checkSelfPermission()
    {
        try
        {
            String temp = "";

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                temp += Manifest.permission.READ_EXTERNAL_STORAGE + " ";
            }

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                temp += Manifest.permission.WRITE_EXTERNAL_STORAGE + " ";
            }
            if (TextUtils.isEmpty(temp) == false)
            {
                ActivityCompat.requestPermissions(this, temp.trim().split(" "),1);
            }
            else {
                openView();
            }
        }
        catch(Exception e)
        {
            io.thebackend.webview.BackendWebView.getInstance().LogError("checkSelfPermission",e.toString());
            e.printStackTrace();
        }
    }
    void openView()
    {
        try
        {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, 1000);
        }
        catch(Exception e)
        {
            io.thebackend.webview.BackendWebView.getInstance().LogError("openView",e.toString());
            e.printStackTrace();
        }
    }

    public void LogError(String _start, String e)
    {
        Log.e(LOGTAG,_start + " : " + e);
    }

}
class ObservableWebView extends WebView {
    private OnScrollChangedCallback mOnScrollChangedCallback;

    public ObservableWebView(final Context context) {
        super(context);
    }

    public ObservableWebView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservableWebView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedCallback != null) mOnScrollChangedCallback.onScroll(l, t);
    }

    public OnScrollChangedCallback getOnScrollChangedCallback() {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback) {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    public static interface OnScrollChangedCallback {
        public void onScroll(int l, int t);
    }
}