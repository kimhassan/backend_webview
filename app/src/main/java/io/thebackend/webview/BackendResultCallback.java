package io.thebackend.webview;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class BackendResultCallback extends Activity {

    public final String LOGTAG = io.thebackend.webview.BackendWebView.LOGTAG + "_OnResult";
    int REQ_CODE_SELECT_IMAGE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try
        {
            super.onCreate(savedInstanceState);
            checkSelfPermission();
        }
        catch(Exception e)
        {
            io.thebackend.webview.BackendWebView.getInstance().LogError("onCreate",e.toString());
            e.printStackTrace();
        }
    }
    void openView()
    {
        try
        {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(intent, REQ_CODE_SELECT_IMAGE);
        }
        catch(Exception e)
        {
            io.thebackend.webview.BackendWebView.getInstance().LogError("openView",e.toString());
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try
        {
            String pathFinder = null;
            String name = null;
            String size = null;
            String extension = null;
            byte[] str = null;
            if (resultCode == Activity.RESULT_OK)
            {
                Uri contentURI = data.getData();
                Uri queryUri = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                    queryUri = MediaStore.Files.getContentUri("external");
                }
                String columnData = MediaStore.Files.FileColumns.DATA;
                String columnSize = MediaStore.Files.FileColumns.SIZE;

                String[] projectionData = {MediaStore.Files.FileColumns.DATA};

                Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
                if ((cursor != null)&&(cursor.getCount()>0)) {
                    int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE);

                    cursor.moveToFirst();

                    name = cursor.getString(nameIndex);

                    size = cursor.getString(sizeIndex);

                    cursor.close();
                }

                if ((name!=null)&&(size!=null)){
                    String selectionNS = columnData + " LIKE '%" + name + "' AND " +columnSize + "='" + size +"'";
                    Cursor cursorLike = getContentResolver().query(queryUri, projectionData, selectionNS, null, null);
                    String result = null;
                    if ((cursorLike != null)&&(cursorLike.getCount()>0)) {
                        cursorLike.moveToFirst();
                        int indexData = cursorLike.getColumnIndex(columnData);
                        if (cursorLike.getString(indexData) != null) {
                            result = cursorLike.getString(indexData);
                            pathFinder = result;
                        }
                        cursorLike.close();
                    }
                }
            }
            finish();
            double memory = Double.valueOf(size) / 1000000;
            size = String.format("%.2f",memory);
            String split[] = name.split("[.]");
            extension = split[split.length-1];
            io.thebackend.webview.BackendWebView.getInstance().webView.loadUrl("javascript:getFilesInfo( '"+ pathFinder +"' ,'"+ name +"' ,'"+size+"' ,'"+extension+"' );") ;
        }
        catch(Exception e)
        {
            finish();
            io.thebackend.webview.BackendWebView.getInstance().LogError("onActivityResult",e.toString());
            e.printStackTrace();

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) { //권한을 허용 했을 경우
        try
        {
            boolean isREADSTORAGE = false;
            boolean isWRITESTORAGE = false;

            if(requestCode == 1)
            {
                int length = permissions.length;
                for (int i = 0; i < length; i++)
                {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED)
                    { // 동의
                        if(permissions[i].equals("android.permission.READ_EXTERNAL_STORAGE")) {
                            isREADSTORAGE = true;

                        }
                        if(permissions[i].contains("android.permission.WRITE_EXTERNAL_STORAGE"))
                        {
                            isWRITESTORAGE = true;
                        }
                    }
                }
                if(isREADSTORAGE && isWRITESTORAGE)
                {
                    openView();
                }
                else
                {
                    finish();
                }
            }
        }
        catch(Exception e) {
            io.thebackend.webview.BackendWebView.getInstance().LogError("onRequestPermissionsResult",e.toString());
            e.printStackTrace();
        }
    }
    public void checkSelfPermission()
    {
        try
        {
            String temp = "";

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                temp += Manifest.permission.READ_EXTERNAL_STORAGE + " ";
            }

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
            {
                temp += Manifest.permission.WRITE_EXTERNAL_STORAGE + " ";
            }
            if (TextUtils.isEmpty(temp) == false)
            {
                ActivityCompat.requestPermissions(this, temp.trim().split(" "),1);
            }
            else {
                openView();
            }
        }
        catch(Exception e)
        {
            io.thebackend.webview.BackendWebView.getInstance().LogError("checkSelfPermission",e.toString());
            e.printStackTrace();
        }
    }
}
